#include "stdafx.h"
#include <vector>
#include <algorithm>

using namespace std;

class MinPathSum {
public:
	void dp(vector<vector<int>>& grid, int x, int y, vector<vector<int>>& cache){
		if (x == grid.size() - 1 && y == grid[0].size() - 1){
			cache[x][y] = grid[x][y];
			return;
		}

		if (y + 1 < grid[0].size() && cache[x][y + 1] == -1){
			dp(grid, x, y + 1, cache);
		}
		if (x + 1 < grid.size() && cache[x + 1][y] == -1){
			dp(grid, x + 1, y, cache);
		}

		if (x == grid.size() - 1){
			cache[x][y] = grid[x][y] + cache[x][y + 1];
			return;
		}
		if (y == grid[0].size() - 1){
			cache[x][y] = grid[x][y] + cache[x + 1][y];
			return;
		}
		cache[x][y] = grid[x][y] + min(cache[x][y + 1], cache[x + 1][y]);
	}

	int minPathSum(vector<vector<int>>& grid) {
		if (grid.empty()) return 0;

		vector<vector<int>> cache(grid.size(), vector<int>(grid[0].size(), -1));
		dp(grid, 0, 0, cache);

		return cache[0][0];
	}
};