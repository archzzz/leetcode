#include "stdafx.h"
#include <map>
#include <vector>

using namespace std;

class ValidSoduku {
public:
	bool isValidSudoku(vector<vector<char>>& board) {
		map<char, map<string, map<int, bool>>> suduku;
		for (int i = 0; i < 9; i++){
			for (int j = 0; j < 9; j++){
				if (board[i][j] != '.'){
					if (suduku[board[i][j]]["row"][i] == 0){
						suduku[board[i][j]]["row"][i] = true;
					}
					else {
						return false;
					}
					if (suduku[board[i][j]]["col"][j] == 0){
						suduku[board[i][j]]["col"][j] = true;
					}
					else {
						return false;
					}
					int grid = 3 * (i / 3) + (j / 3);
					if (suduku[board[i][j]]["grid"][grid] == 0){
						suduku[board[i][j]]["grid"][grid] = true;
					}
					else {
						return false;
					}
				}
			}
		}
		return true;
	}
};