#include "stdafx.h"
#include <vector>

using namespace std;

class NextPermutation {
public:
	void swap(vector<int>& nums, int a, int b){
		int temp = nums[a];
		nums[a] = nums[b];
		nums[b] = temp;
	}

	void inverse(vector<int>& nums, int a, int b){
		while (a < b){
			swap(nums, a, b);
			a++;
			b--;
		}
	}

	void nextPermutation(vector<int>& nums) {
		if (nums.size() < 2) return;

		int i = nums.size() - 1;
		while (i > 0 && nums[i - 1] >= nums[i]) i--;   //max form the end

		if (i == 0) {    // cannot rearrange
			inverse(nums, 0, nums.size() - 1);
			return;
		}

		i--;        //next num less than max
		int j = nums.size() - 1;
		while (i < j && nums[i] >= nums[j]) j--;

		swap(nums, i, j);

		i++;
		j = nums.size() - 1;

		inverse(nums, i, j);

	}
};