#include "stdafx.h"
#include <string>
#include <map>

using namespace std;

class ValidAnagram {
public:
	bool isAnagram(string s, string t) {
		if (s.length() != t.length()) return false;

		map<char, int> counter;
		for (int i = 0; i < s.length(); i++){
			counter[s[i]]++;
			counter[t[i]]--;
		}
		for (auto it : counter){
			if (it.second != 0) return false;
		}
		return true;
	}
};