#include "stdafx.h"
#include <vector>

class MissingNumber {
public:
	int missingNumber(std::vector<int>& nums) {
		int result = 0;
		for (int num : nums){
			result += num;
		}
		int n = nums.size();
		return (n + 1)*n*0.5 - result;
	}
};