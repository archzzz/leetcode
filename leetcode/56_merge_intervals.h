#include "stdafx.h"
#include <vector>
#include <algorithm>

using namespace std;

/* Definition for an interval. */
struct Interval {
     int start;
     int end;
     Interval() : start(0), end(0) {}
     Interval(int s, int e) : start(s), end(e) {}
 };

class MergeIntervals {
public:
	vector<Interval> merge(vector<Interval>& intervals) {
		if (intervals.size() < 2) return intervals;

		sort(intervals.begin(), intervals.end(), [](Interval& a, Interval& b){ return a.start < b.start; });

		vector<Interval> result;
		int start = intervals.front().start, end = intervals.front().end;
		for (auto it : intervals){
			if (it.start <= end){
				end = max(it.end, end);
			}
			else{
				result.push_back(Interval(start, end));
				start = it.start;
				end = it.end;
			}
		}
		result.push_back(Interval(start, end));
		return result;
	}
};