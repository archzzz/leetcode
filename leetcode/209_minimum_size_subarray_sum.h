#include "stdafx.h"
#include <vector>
#include <algorithm>

using namespace std;

class MinSizeSubarraySum {
public:
	int minSubArrayLen(int s, vector<int>& nums) {
		if (nums.empty()) return 0;

		int left = 0, right = 0, sum = 0, size = nums.size(), minLen = size + 1;

		while (right < size){
			while (sum < s && right < size){
				sum = sum + nums[right++];
			}
			while (sum >= s && left < right){
				sum = sum - nums[left++];
			}
			minLen = min(right - left + 1, minLen);
		}
		return minLen == size + 1 ? 0 : minLen;
	}
};