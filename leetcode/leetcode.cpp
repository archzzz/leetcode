// leetcode.cpp : Defines the entry point for the console application.
//

//array
#include "stdafx.h"
#include "268_missing_number.h"
#include "31_next_permutation.h"
#include "209_minimum_size_subarray_sum.h"
#include "64_minium_path_sum.h"
#include "56_merge_intervals.h"
//hash table
#include "290_word_pattern.h"
#include "36_valid_soduku.h"
#include "242_valid_anagram.h"
#include "136_single_number.h"
#include "187._repeated_dna_sequences.h"


int main(int argc, char* argv[])
{
	return 0;
}

