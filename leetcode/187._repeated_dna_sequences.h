#include "stdafx.h"
#include <vector>
#include <map>
#include <inttypes.h>

using namespace std;

class RepeatedDNA {
public:
	vector<string> findRepeatedDnaSequences(string s) {
		vector<string> result;
		if (s.length() < 11) return result;

		map<int32_t, int> sequences;

		int32_t str;
		for (int i = 0; i < 10; i++){
			str = (str << 2) | ((s[i] - 'A') % 5);
		}
		sequences[str]++;
		for (int i = 1; i < s.length() - 9; i++){
			str = str  & (1 << 18);
			str = (str << 2) | ((s[i + 9] - 'A') % 5);
			if ((++sequences[str]) == 2){
				result.push_back(s.substr(i, 10));
			}
		}
		return result;
	}
};

