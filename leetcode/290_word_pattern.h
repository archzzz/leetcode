#include "stdafx.h"
#include <string>
#include <map>
#include <sstream>

using namespace std;

class WordPattern {
public:
	bool wordPattern(string pattern, string str) {
		map<char, string> c2str;
		map<string, char> str2c;

		istringstream line(str);
		istringstream patt(pattern);

		char c;
		string token;

		map<char, string>::iterator charptr;
		map<string, char>::iterator strptr;

		while (line && patt){
			line >> token;
			patt >> c;
			charptr = c2str.find(c);
			strptr = str2c.find(token);
			if (charptr == c2str.end() && strptr == str2c.end()){
				c2str[c] = token;
				str2c[token] = c;
				continue;
			}
			else if (charptr != c2str.end()){
				if (charptr->second == token){
					continue;
				}
			}
			return false;
		}
		return (line || patt) ? false : true;
	}
};